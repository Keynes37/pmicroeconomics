# Notas de clase del curso #

Las presentaciones del curso se iran colgando en esta parte, para que los estudiantes del curso puedan consultarlo.

### Clases ###

* Introducción [Clase 1](https://bb.githack.com/Keynes37/pmicroeconomics/raw/main/Files/Clase%201/Class1.html)

* Demanda [Clase 2](https://bb.githack.com/Keynes37/pmicroeconomics/raw/main/Files/Clase%202/Class2.html)

* Elasticidad de la demanda [Clase 3](https://bb.githack.com/Keynes37/pmicroeconomics/raw/main/Files/Clase%203/Clase-03.html)

* Preferencias y elección [Clase 4](https://bb.githack.com/Keynes37/pmicroeconomics/raw/main/Files/Clase%204/Clase04.html)

* Presupuesto [Clase 5](https://bb.githack.com/Keynes37/pmicroeconomics/raw/main/Files/Clase%205/Clase05.html)

* Consumo ingreso [Clase 6](https://bb.githack.com/Keynes37/pmicroeconomics/raw/main/Files/Clase%206/Clase06.html)

* Teoría del Productor [Clase 7](https://bb.githack.com/Keynes37/pmicroeconomics/raw/main/Files/Clase%207/Clase07.html)

* Teoría del Productor II [Clase 8](https://bb.githack.com/Keynes37/pmicroeconomics/raw/main/Files/Clase%208/Clase08.html)

* Teoría de Costos [Clase 9](https://bb.githack.com/Keynes37/pmicroeconomics/raw/main/Files/Clase%209/Clase09.html)

* Teoría de Beneficios [Clase 10](https://bb.githack.com/Keynes37/pmicroeconomics/raw/main/Files/Clase%2010/Clase10.html)

* Mercado Monopolio[Clase 11](https://bb.githack.com/Keynes37/pmicroeconomics/raw/main/Files/Clase%2011/Clase11.html)

* Mercado Oligopolio [Clase 12](https://bb.githack.com/Keynes37/pmicroeconomics/raw/main/Files/Clase%2012/Clase12.html)